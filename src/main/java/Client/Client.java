package Client;

import io.grpc.*;

import io.grpc.stub.StreamObserver;
import resouces.protos.chat.ChatMessage;
import resouces.protos.chat.ChatMessageFromServer;
import resouces.protos.chat.ChatServiceGrpc;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.util.Scanner;

import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class Client {
    private static final Logger logger = Logger.getLogger(Client.class.getName());

    private final ChatServiceGrpc.ChatServiceStub blockingStub;

    public Client(Channel channel) {
        blockingStub = ChatServiceGrpc.newStub(channel);
    }

    public void sendMessage(String name) {
        System.out.println("Introduceti mesajul mai jos: ");

        StreamObserver<ChatMessage> requestObserver =
                blockingStub.chat(new StreamObserver<ChatMessageFromServer>() {
                    @Override
                    public void onNext(ChatMessageFromServer response) {
                        if (response.getMessage().getFrom().equals(name)) {
                            System.out.println("eu" +" ==> "+ response.getMessage().getMessage());
                        }
                        else {
                            System.out.println(response.getMessage().getFrom() +" ==> "+ response.getMessage().getMessage());
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        Status status = Status.fromThrowable(t);
                        System.err.println("RouteChat Failed: {0}"+status);
                    }

                    @Override
                    public void onCompleted() {
                        System.out.println("Server sent END");
                    }
                });

        try {
            InputStream is = System.in;
            BufferedReader br =  new BufferedReader(new InputStreamReader(is));
            String line = null;
            while ((line = br.readLine()) != null) {
                if (line.equalsIgnoreCase("q")) {
                    break;
                }
                requestObserver.onNext(ChatMessage
                        .newBuilder()
                        .setMessage(line)
                        .setFrom(name)
                        .build());
            }
        }catch(Exception e){
            System.out.println("Exception occured:"+e);
            requestObserver.onError(e);

        }
    }

    public static void main(String[] args) throws Exception {
        String name;
        String target = "localhost:50051";

        ManagedChannel channel = ManagedChannelBuilder
                .forTarget(target)
                .usePlaintext()
                .build();

        boolean onClient = true;
        while (onClient) {
            System.out.println("Introduceti numele utilizatorului: ");
            Scanner scanner = new Scanner(System.in);
            name = scanner.nextLine();

            Client client = new Client(channel);

            client.sendMessage(name);
        }
        channel.shutdownNow().awaitTermination(5, TimeUnit.SECONDS);
    }
}